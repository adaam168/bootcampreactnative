var sayHello = "Hello World!"
console.log(sayHello)
//program hello world

var name ="Philipp"
var angka=19
var TodayisFriday=false
//console.log untuk memanggil variabel
//di cmder ketik node <nama file> maka hello world akan run
//membuat variabel di javascript
console.log(name)
console.log(angka)
console.log(TodayisFriday)
//hanya string yg menggunakan tanda "" / ''
//angka dan kondisi true|false tidak memakai kutip
//jangan pernah membuat variabel tak bernilai karena hasilnya tidak akan terdefinisi

var sentences = "Javascript"
console.log(sentences[0])
console.log(sentences[2])
//hasilnya adalah J dan V, sentences[x] string dimulai dari 0 seperti PHP array

var word = "Javascript is awesome"
console.log(word.length)
//hasilnya adalah 21, word.length untuk mencari tahu berapa panjang karakter

console.log('i am a string'.charAt(3));
//string bisa langsung dideklarasikan di console.log tanpa var terlebih dahulu
//hasilnya adalah m, charAt untuk mencari tahu posisi karakter

var string1 = 'Assalamualaikum,';
var string2 = 'teman-teman';
console.log(string1.concat(string2)); // goodluck
//hasilnya adalah Assalamualaikum teman-teman, concat menggabung 2 string
//menjadi 1 string baru

var motor1 = 'asep motor';
var motor2 = motor1.substr(1, 3);
console.log(motor2);
//hasilnya adalah sep, substr adalah memotong string sesuai indeks
//cara penulisanya adalah substr(<indeks awal>,<karakter yg akan diambil)

var letter = 'This Letter Is For You';
var result  = letter.toUpperCase();
console.log(result);
//merubah string menjadi huruf kapital semua

var letter = 'This Letter Is For You';
var result  = letter.toLowerCase();
console.log(result);
//kebalikan dari toUpperCase()

var username    = ' mamanracing ';
var newUsername = username.trim();
console.log(newUsername)
//hasilnya menjadi 'mamanracing', trim menghilangkan whitespace (spasi)

var int  = 12;
var real = 3.45;
var arr  = [6, 7, 8];

var strInt  = String(int);
var strReal = String(real);
var strArr  = String(arr);

console.log(strInt);
console.log(strReal);
console.log(strArr);
//String() merubah tipe data menjadi string
//jika array dikonversi makan dipisahkan dengan koma ','
var number = 21;
console.log(number.toString());
var array = [1,2];
console.log(array.toString());
//.toString() merubah tipe data menjadi string tanpa mendeklarasikan ulang seperti String()

var number1 = Number("90");
var number2 = Number("1.23");
var number3 = Number("4 5");
console.log(number1);
console.log(number2);
console.log(number3);
//Number() merubah tipe data string menjadi angka, data yg dikonversi harus berbentuk menghilangkan
//jika tidak maka menghasilkan Nan (Not an Number/Bukan angka)
//jika yg ingin dikonversi berupa bilangan desimal, gunakan titik',' sebagai pemisah

var int  = '89';
var real = '56.7';
var strInt_1 = parseInt(int);
var strInt_2 = parseInt(real);
var strReal_1 = parseFloat(int);
var strReal_2 = parseFloat(real);
console.log(strInt_1);
console.log(strInt_2);
console.log(strReal_1);
console.log(strReal_2);
//
//

var minimarketStatus = "open"
if ( minimarketStatus == "open" ) {
    console.log("saya akan membeli telur dan buah")
} else {
    console.log("minimarketnya tutup")
}
//contoh kondisional sederhana, hasilnya adalah open

var minimarketStatus = "close"
var minuteRemainingToOpen = 5
if ( minimarketStatus == "open" ) {
    console.log("saya akan membeli telur dan buah")
} else if ( minuteRemainingToOpen <= 5 ) {
    console.log("minimarket buka sebentar lagi, saya tungguin")
} else {
    console.log("minimarket tutup, saya pulang lagi")
}
//contoh kondisional dengan menambahkan kondisi tambahan
//yaitu minuteRemainingToOpen

var minimarketStatus = "open"
var telur = "soldout"
var buah = "soldout"
if ( minimarketStatus == "open" ) {
    console.log("saya akan membeli telur dan buah")
    if(telur == "soldout" || buah == "soldout" ) {
        console.log("belanjaan saya tidak lengkap")
    } else if( telur == "soldout") {
        console.log("telur habis")
    } else if ( buah == "soldout" ) {
        console.log("buah habis")
    }
} else {
    console.log("minimarket tutup, saya pulang lagi")
}
//contoh kondisional bersarang
//didalam kondisi ada kondisi lagi

var buttonPushed = 1;
switch(buttonPushed) {
  case 1:   { console.log('matikan TV!'); break; }
  case 2:   { console.log('turunkan volume TV!'); break; }
  case 3:   { console.log('tingkatkan volume TV!'); break; }
  case 4:   { console.log('matikan suara TV!'); break; }
  default:  { console.log('Tidak terjadi apa-apa'); }}
//contoh kondisional switch case, mirip seperti sebuah remote
//case yg terpilih maka case itu hasilnya
//hasil dari contoh ini adalah case no 1
