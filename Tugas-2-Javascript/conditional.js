//Jawaban tugas 2 conditional

//Soal if-else
var nama = "Philipp"
var peran = "Knight"
 if(nama=="John")
{
  console.log("Halo John, pilih peranmu untuk memulai game")
}
  else if(nama=="Philipp" || peran=="Knight")
  {
    console.log("Selamat datang di dunia werewolf, Philipp")
    console.log("Halo Knight Philipp, kamu akan membantu melindungi temanmu dari werewolf")
  }
  else if(nama=="Jane" || peran=="Mage")
  {
    console.log("Selamat datang di dunia werewolf, Jane")
    console.log("Halo Jane, kamu dapat melihat siapa yang menjadi werewolf")
  }
  else if(nama=="Romulus" || peran=="Werewolf")
  {
    console.log("Selamat datang di dunia werewolf, Romulus")
    console.log("Halo Romulus, Kamu harus memangsa semua orang")
  }
else
{
  console.log("Nama dan peran harus diisi")
}
//hasilnya adalah sesuai kondisi, disini hasilnya adalah nama Philipp dan peran Knight

//Soal switch
var hari = 1;
var bulan = 5;
var tahun = 1945
switch(true)
{
  case(hari >= 1  && hari <= 31): { console.log(hari); break; }
  default :  { console.log('Isi tanggal disni'); }
}
switch(bulan) {
  case 1:   { console.log('Januari'); break; }
  case 2:   { console.log('Februari'); break; }
  case 3:   { console.log('Maret'); break; }
  case 4:   { console.log('April'); break; }
  case 5:   { console.log('Mei'); break; }
  case 6:   { console.log('Juni'); break; }
  case 7:   { console.log('Juli'); break; }
  case 8:   { console.log('Agustus'); break; }
  case 9:   { console.log('September'); break; }
  case 10:   { console.log('Oktober'); break; }
  case 11:   { console.log('November'); break; }
  case 12:   { console.log('Desember'); break; }
  default :  { console.log('Isi bulan disni'); }
}
switch(true)
{
  case(tahun >= 1900 && tahun <= 2200): {console.log(tahun); break;}
  default: {console.log('Masukan tahun disini'); }
}
//hasilnya adalah 1 Mei 1945
