//Tugas 3 Looping
//Soal No 1
//Looping pertama
console.log('LOOPING PERTAMA');
var deret = 2;
var jumlah = 0;
while(jumlah < 20){
  jumlah += deret;
  deret+
  console.log(+jumlah+' - I love coding');
}
//looping kedua
console.log('LOOPING KEDUA');
var deret = 2;
var jumlah = 22;
while(jumlah > 2){
  jumlah -= deret;
  deret-
  console.log(+jumlah+' - I will become a mobile developer');
}

//Soal No 2
console.log('\n');
//Looping for
for(var deret = 1; deret < 20; deret++){
  if(deret % 2 === 0){
    console.log(+deret+' Berkualitas');
  }else if(deret % 3 === 0){
    console.log(+deret+' I love coding');
  }else{
    console.log(+deret+ ' Santai');
  }
}

//Soal No 3
console.log('\n')
//Persegi Panjang
var flag = 1;
while(flag < 5){
  console.log('########');
  flag++;
}

//Soal No 4
console.log('\n')
//Tangga
for(var a=1; a<7; a++){
  for(var b=1; b<a; b++){
      process.stdout.write('#')
  }
  console.log('\n');
}

//Soal No 5
console.log('\n')
//Papan Catur
for(var a=1; a<9; a++){
  if(a % 2 === 1){
    process.stdout.write('#')
  }
  else{
    process.stdout.write('')
  }
  //console.log('\n')
  for(var b=1; b<9; b++){
    if(b % 2 === 1){
    process.stdout.write(' ')
    }
  else{
    process.stdout.write('#')
    }
  }
  console.log('\n')
}
